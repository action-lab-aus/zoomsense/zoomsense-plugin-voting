module.exports = {
  configureWebpack: {
    externals: ["firebase", "vuefire", "quasar"],
  },
  css: {
    extract: false
  },
};
