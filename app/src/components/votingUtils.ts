import { VotingItem, VotingPluginConfig, VotingPluginMeeting } from "zoomsense-plugin-voting-common";

const defaultVoteTypeName = "strategy/strategies";

export function getVoteTypeName(votingConfig?: VotingPluginConfig): undefined | { singular: string; plural: string } {
  const voteTypeName = votingConfig?.voteTypeName ?? defaultVoteTypeName;
  const singularPlural = voteTypeName.split("/");
  const singular = singularPlural[0];
  if (!singular) {
    return undefined;
  }
  return { singular, plural: singularPlural[1] ?? `${singular}s` };
}

export function areVotesRatings(votingConfig?: VotingPluginConfig) {
  return votingConfig?.type === "rating";
}

export function getVoteTotals(meetingVotes: VotingPluginMeeting, item = meetingVotes.currentItem) {
  return Object.values(meetingVotes.items?.[item!]?.votes ?? {}).reduce((totals, vote) => {
    totals[vote.votingResult] = (totals[vote.votingResult] ?? 0) + 1;
    return totals;
  }, {} as { [votingResult: string]: number });
}

const voteValues: { [votingResult: string]: number } = {
  no: -1,
  yes: 1,
  rating1: 1,
  rating2: 2,
  rating3: 3,
  rating4: 4,
  rating5: 5,
};

export interface VotingItemData extends VotingItem {
  itemId: string;
  voteTotals: { [votingResult: string]: number };
  score: string;
}

export function getVotingItemData(meetingVotes: VotingPluginMeeting): VotingItemData[] {
  return Object.entries(meetingVotes.items ?? {}).map(([itemId, { item }]) => {
    const voteTotals = getVoteTotals(meetingVotes, itemId);
    const { score, votes, rating } = Object.keys(voteTotals).reduce(
      (params, votingResult) => ({
        score: params.score + (voteValues[votingResult] ?? 0) * (voteTotals[votingResult] ?? 0),
        votes: params.votes + (voteTotals[votingResult] ?? 0),
        rating: params.rating || votingResult.startsWith("rating"),
      }),
      { score: 0, votes: 0, rating: false }
    );
    return {
      itemId,
      ...item,
      voteTotals,
      score: rating ? (votes > 0 ? (score / votes).toFixed(2) : "0.00") : String(score),
    };
  });
}
