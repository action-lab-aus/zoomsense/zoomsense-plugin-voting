import * as functions from "firebase-functions";
import { defaultSubmissionName, defaultVoteType, voteTypeValidValues } from "zoomsense-plugin-voting-common";
import { typedFirebaseFunctionRef } from "@zoomsense/zoomsense-firebase";

import { db } from "./db";

/**
 * When a new message `messageId` with a vote is added under `data/chats`, fetch the plugins for the current
 * section from /config and save the vote for the current strategy if voting is enabled.
 * The metadata for the voter is stored under `/data/voting/${meetingId}/current/${currentVoteItemId}/meta` node.
 * Every student is only allowed 1 vote per strategy.
 *
 * @param {String} meetingId Meeting ID
 */
exports.captureVotes = typedFirebaseFunctionRef(functions.database.ref)(
  "data/chats/{meetingId}/ZoomSensor_1/{messageId}",
).onCreate(async (snapshot, context) => {
  const meetingId = context.params.meetingId;
  const message = snapshot.val();

  if (!message) return;

  const votingConfig = (await db.ref(`config/${meetingId}/current/currentState/plugins/voting`).once("value")).val();
  if (!votingConfig?.enabled) {
    return;
  }
  const submissionName = votingConfig.submissionName || defaultSubmissionName;

  const currentVoteItemId = (await db.ref(`data/plugins/voting/${meetingId}/${submissionName}/currentItem`).once("value")).val();
  if (!currentVoteItemId || message.msgReceiverName !== "ZoomSensor_1") {
    return;
  }

  const validResponses = voteTypeValidValues[votingConfig.type!] || voteTypeValidValues[defaultVoteType];
  const chatText = message.msg.toLowerCase().trim();

  // Make the valid responses a lookup table to see if the chat text is a valid response
  const votingResult = (validResponses as Record<string, string>)[chatText];

  if (!votingResult) {
    return;
  }

  // If the user casting the vote has already voted, this a ref to that vote
  const usersPreviousVoteRef = db.ref(
    `data/plugins/voting/${meetingId}/${submissionName}/items/${currentVoteItemId}/votes/${message.msgSender}`,
  );
  const usersPreviousVote = await usersPreviousVoteRef.get();

  // The user hasn't voted before, so let's create a vote for them
  if (!usersPreviousVote.exists()) {
    // TODO it would be nice to notify the user that their vote has been registered with a private message.
    await usersPreviousVoteRef.set({
      timestamp: message.timestamp,
      userName: message.msgSenderName,
      votingResult,
    });
    return;
  }

  const previousVoteValue = usersPreviousVote.val()!;
  const existingId = Object.keys(previousVoteValue)[0];
  if (!existingId) return;
  const previousVotingResult = previousVoteValue!.votingResult;

  if (votingConfig.canChangeVote && previousVotingResult !== votingResult) {
    // TODO it would be nice to notify the user that their vote has been updated
    await usersPreviousVoteRef.update({
      timestamp: message.timestamp,
      votingResult,
    });
    return;
  }

  // TODO maybe we could notify them that they can't change their vote
});
