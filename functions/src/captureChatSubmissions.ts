import { typedFirebaseFunctionRef } from "@zoomsense/zoomsense-firebase";
import * as functions from "firebase-functions";
import { db } from "./db";
import { defaultSubmissionName } from "zoomsense-plugin-voting-common";

/**
 * If new chat submissions arrive while the voting plugin is enabled, check for matches and add them.
 */
export const captureChatSubmissions = typedFirebaseFunctionRef(functions.database.ref)(
  "data/plugins/chatSubmission/{meetingId}/{messageId}",
).onCreate(async (snapshot, context) => {
  const meetingId = context.params.meetingId;
  const messageId = context.params.messageId;
  const chatSubmission = snapshot.val();

  if (!chatSubmission) {
    return;
  }
  const votingConfig = (await db.ref(`config/${meetingId}/current/currentState/plugins/voting`).once("value")).val();
  if (!votingConfig?.enabled) {
    return;
  }
  const submissionName = votingConfig.submissionName || defaultSubmissionName;
  if (submissionName !== chatSubmission.submissionName) {
    return;
  }

  // A matching chatSubmission record - copy it over into our voting data.
  await db.ref(`data/plugins/voting/${meetingId}/${submissionName}/items`).push({
    item: {
      itemName: chatSubmission.submission,
      timestamp: chatSubmission.timestamp,
      userName: chatSubmission.senderName,
      chatSubmissionId: messageId,
    }
  });
});

/**
 * If the voting plugin becomes enabled, check for any existing chatSubmissions for this meeting from previous script
 * stages which match the now-enabled config.
 */
export const backdateChatSubmissions = typedFirebaseFunctionRef(functions.database.ref)(
  "config/{meetingId}/current/currentState/plugins/voting",
).onCreate(async (snapshot, context) => {
  const votingConfig = snapshot.val();
  if (!votingConfig?.enabled) {
    return;
  }
  const submissionName = votingConfig.submissionName || defaultSubmissionName;
  const meetingId = context.params.meetingId;
  const chatSubmissions = (await db.ref(`data/plugins/chatSubmission/${meetingId}`).get()).val();
  if (chatSubmissions) {
    const meetingItemsRef = db.ref(`data/plugins/voting/${meetingId}/${submissionName}/items`);
    // Build a map of existing ids, so we don't copy submissions more than once (e.g. if the host manually re-enters
    // this section).
    const existingItemsSnapshot = await meetingItemsRef.once("value");
    const existingIds = Object.fromEntries(
      Object.values(existingItemsSnapshot.val() ?? {})
        .map(({ item }) => ([item.chatSubmissionId, true]))
    );
    for (const messageId in chatSubmissions) {
      const chatSubmission = chatSubmissions[messageId]!;
      if (submissionName === chatSubmission.submissionName && !existingIds[messageId]) {
        // A matching chatSubmission record - copy it over into our voting data.
        await meetingItemsRef.push({
          item: {
            itemName: chatSubmission.submission,
            timestamp: chatSubmission.timestamp,
            userName: chatSubmission.senderName,
            chatSubmissionId: messageId,
          }
        });
      }
    }
  }

});