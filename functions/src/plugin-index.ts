// This file should only be used when emulating the firebase functions locally.  Change `../package.json` so that `main`
// points to this file instead of the real index.ts

// The plugin architecture exposes all of a plugin's Firebase functions under the plugin name
// (The plugin name is the folder name created in the Plugin Directory project which references this plugin.)
export * as Voting from "./index";
// Also export the core ZoomSense functions, so the emulator has them all.
// @ts-ignore
export * from "../../../zoomsense-functions/functions/lib/index";