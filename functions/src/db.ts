import * as admin from "firebase-admin";
import { ZoomSenseServerFirebaseDB } from "@zoomsense/zoomsense-firebase";
import "zoomsense-plugin-voting-common";

export const db = new ZoomSenseServerFirebaseDB(admin.database());
