import { db } from '../src/db';

const TEST_MEETING_ID = 'test';

describe('voting', () => {
  it('should work', async () => {
    await db.ref(`/config/${TEST_MEETING_ID}/current/currentState/plugins/voting`).set({
      enabled: true,
      type: 'yes-no',
      submissionName: 'test submission',
      canChangeVote: false,
      voteTypeName: 'test',
    });
  });
});
