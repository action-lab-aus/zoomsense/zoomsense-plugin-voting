import * as initTest from "firebase-functions-test";
import * as admin from "firebase-admin";

// User emulator database
export const databaseURL = "http://localhost:9000/?ns=zoomsense-plugin-default-rtdb";

const projectConfig = {
  projectId: "zoomsense-plugin",
  databaseURL,
};

export const testRunner = initTest(projectConfig);

admin.initializeApp({ databaseURL });

beforeEach(() => admin.database().ref("/").remove());
