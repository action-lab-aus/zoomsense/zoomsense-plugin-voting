import { makeTypedTestDataSnapshot } from "@zoomsense/zoomsense-firebase";
import * as chai from "chai";
import { VotingPluginConfig } from "zoomsense-plugin-voting-common";

import { testRunner } from "./setup";
import * as captureChatSubmissions from "../src/captureChatSubmissions";
import { db } from "../src/db";

describe("chat submission integration", () => {

  const meetingId = "test-meeting-id-123";
  const submissionName = "targetSubmissionName";

  describe("when added while voting is active", () => {

    const wrappedCaptureChatSubmissions = testRunner.wrap(captureChatSubmissions.captureChatSubmissions);
    const messageId = "message-id";

    it("should copy matching new submissions", async () => {
      const submission = "strategy: Speak softly.";

      await db.ref(`config/${meetingId}/current/currentState/plugins/voting`).set({
        enabled: true,
        submissionName
      });

      await wrappedCaptureChatSubmissions(makeTypedTestDataSnapshot(
        testRunner,
        `data/plugins/chatSubmission/${meetingId}/${messageId}`,
        {
          section: 0,
          timestamp: Date.now(),
          sensor: "ZoomSensor_1",
          senderId: 1234,
          senderName: "Student A",
          submissionPrefix: "strategy",
          submissionName,
          submission
        }
      ), { params: { meetingId, messageId } });

      const items = (await db.ref(`data/plugins/voting/${meetingId}/${submissionName}/items`).get()).val()!;
      chai.assert.exists(items);
      chai.assert.lengthOf(Object.keys(items), 1);
      const key = Object.keys(items)[0]!;
      chai.assert.equal(items[key]!.item.itemName, submission);
    });

    it("should not copy submissions that don't match", async () => {
      await db.ref(`config/${meetingId}/current/currentState/plugins/voting`).set({
        enabled: true,
        submissionName
      });

      await wrappedCaptureChatSubmissions(makeTypedTestDataSnapshot(
        testRunner,
        `data/plugins/chatSubmission/${meetingId}/${messageId}`,
        {
          section: 0,
          timestamp: Date.now(),
          sensor: "ZoomSensor_1",
          senderId: 1234,
          senderName: "Student A",
          submissionPrefix: "strategy",
          submissionName: "not" + submissionName,
          submission: "notStrategy: Check your return values for errors."
        }
      ), { params: { meetingId, messageId } });

      const items = (await db.ref(`data/plugins/voting/${meetingId}/${submissionName}/items`).get()).val()!;
      chai.assert.notExists(items);
    });

  });

  describe("when starting a new section", () => {

    const backdateChatSubmissions = testRunner.wrap(captureChatSubmissions.backdateChatSubmissions);

    it("should copy matching existing submissions", async () => {
      const submission = "strategy: Run in circles.";
      await db.ref(`data/plugins/chatSubmission/${meetingId}`).push({
        section: 0,
        timestamp: Date.now(),
        sensor: "ZoomSensor_1",
        senderId: 1234,
        senderName: "Student A",
        submissionPrefix: "strategy",
        submissionName,
        submission
      });

      const newSectionConfig = makeTypedTestDataSnapshot(
        testRunner,
        `config/${meetingId}/current/currentState/plugins/voting`,
        { enabled: true, submissionName } as VotingPluginConfig
      );
      await backdateChatSubmissions(newSectionConfig, { params: { meetingId } });

      const items = (await db.ref(`data/plugins/voting/${meetingId}/${submissionName}/items`).get()).val()!;
      chai.assert.exists(items);
      chai.assert.lengthOf(Object.keys(items), 1);
      const key = Object.keys(items)[0]!;
      chai.assert.equal(items[key]!.item.itemName, submission);
    });

    it("should not copy existing submissions that don't match", async () => {
      await db.ref(`data/plugins/chatSubmission/${meetingId}`).push({
        section: 0,
        timestamp: Date.now(),
        sensor: "ZoomSensor_1",
        senderId: 1234,
        senderName: "Student A",
        submissionPrefix: "strategy",
        submissionName: "not" + submissionName,
        submission: "strategy: Talk very loudly."
      });

      const newSectionConfig = makeTypedTestDataSnapshot(
        testRunner,
        `config/${meetingId}/current/currentState/plugins/voting`,
        { enabled: true, submissionName } as VotingPluginConfig
      );
      await backdateChatSubmissions(newSectionConfig, { params: { meetingId } });

      const items = (await db.ref(`data/plugins/voting/${meetingId}/${submissionName}/items`).get()).val();
      chai.assert.notExists(items);
    });

  });

});