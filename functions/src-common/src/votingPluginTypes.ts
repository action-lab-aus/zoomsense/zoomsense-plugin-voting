import { ZoomSensePlugin } from "@zoomsense/zoomsense-firebase";
import { voteTypeValidValues } from "./votingPluginConstants";

/**
 * This interface describes the configuration for this plugin, entered by meeting organisers for a given section of
 * their meeting.
 */
export type VotingPluginConfig = {
  /**
   * The chat submission name (see the Chat Submission plugin) of items to vote on. Defaults to "strategy"
   */
  submissionName?: string;

  /**
   * Whether the participants are voting yes or no ("yes-no"), or giving a rating between 0 and 5 ("rating"). Defaults
   * to "yes-no".
   */
  type?: keyof typeof voteTypeValidValues;

  /**
   * Whether participants are allowed to change their vote or not.  If true, they can send another vote message to
   * cancel their old vote and give a new one.
   */
  canChangeVote?: boolean;

  /**
   * The human-readable name used to describe the items being voted on in the Dashboard and Overlay (e.g. "Select XXX to
   * vote on next", "Top 3 XXXs").  If the plural is not simply the singular followed by an 's', it can be given
   * explicitly separated by a slash, e.g. "strategy/strategies".  Defaults to "strategy/strategies"
   */
  voteTypeName?: string;
} & ZoomSensePlugin;

export interface Vote {
  timestamp: number;
  userName: string;
  votingResult: string;
}

export interface VotingItem {
  itemName: string;
  timestamp: number;
  userName: string;
  chatSubmissionId?: string;
}

export interface VotingPluginItems {
  [votingItemId: string]: {
    item: VotingItem;
    votes?: {
      [userId: string]: Vote;
    };
  };
}

export interface VotingPluginMeeting {
  currentItem?: string;
  numOverlayItems?: number;
  items?: VotingPluginItems;
}

/**
 * This interface describes this plugin's top-level data stored in the RTDB under the path `data/plugins/voting`.  In
 * any given section of a meeting where the voting plugin is enabled and configured, there will be a VotingPluginMeeting
 * node stored under the meeting ID and the submissionName from this section's `VotingPluginConfig` configuration.
 */
export interface VotingPluginCurrentData {
  [meetingId: string]: {
    [submissionName: string]: VotingPluginMeeting;
  };
}
