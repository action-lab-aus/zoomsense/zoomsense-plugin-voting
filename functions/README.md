# Testing

First, start the firebase database emulator:

- `npx firebase emulators:start --only database`

Then, in a different terminal

- `npx mocha --exit`
